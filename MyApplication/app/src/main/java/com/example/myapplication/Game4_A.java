package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Game4_A extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game4__a);

        Bundle b = getIntent().getExtras();
        final String str = b.getString("Subjects");
        final int j = b.getInt("num");
        final ArrayList<String> listHeb = b.getStringArrayList("HebrewList");
        final ArrayList<String> listEng = b.getStringArrayList("EnglishList");

        final TextView subject = findViewById(R.id.text_subject_4a);
        subject.setText("הנושא: " + str);

        ArrayList<String> list3 = new ArrayList<String>();
        list3.add(listEng.get(j).toString());
        Random random = new Random();
        String word1 = listEng.get(random.nextInt(listEng.size()));
        list3.add(word1);
        final String word2 = list3.get(random.nextInt(list3.size()));

        TextView textHebrew = findViewById(R.id.hebrew_word_4a);
        textHebrew.setText(listHeb.get(j).toString());
        TextView textDst = findViewById(R.id.dst_word_4a);
        textDst.setText(word2);


        findViewById(R.id.image_like_4a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!listEng.get(j).toString().equals(word2)) {
                    listHeb.add(listHeb.get(j));
                    listEng.add(listEng.get(j));
                    Toast.makeText(Game4_A.this, "תזכורת! " + listHeb.get(j).toString() + "=" +  listEng.get(j).toString(), Toast.LENGTH_LONG).show();
                }
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", str);
                bundle.putInt("num", j + 1);
                bundle.putStringArrayList("HebrewList", listHeb);
                bundle.putStringArrayList("EnglishList", listEng);
                Intent intent = new Intent(Game4_A.this, Game4_A.class);
                intent.putExtras(bundle);
                startActivity(intent);

                if (j == listEng.size() - 1) {
                    bundle = new Bundle();
                    bundle.putString("Subjects", str);
                    intent = new Intent(Game4_A.this, Games.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

        findViewById(R.id.image_unlike_4a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listEng.get(j).toString().equals(word2)) {
                    listHeb.add(listHeb.get(j));
                    listEng.add(listEng.get(j));
                    Toast.makeText(Game4_A.this, "תזכורת! " + listHeb.get(j).toString() + "=" +  listEng.get(j).toString(), Toast.LENGTH_SHORT).show();
                }
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", str);
                bundle.putInt("num", j + 1);
                bundle.putStringArrayList("HebrewList", listHeb);
                bundle.putStringArrayList("EnglishList", listEng);
                Intent intent = new Intent(Game4_A.this, Game4_A.class);
                intent.putExtras(bundle);
                startActivity(intent);

                if (j == listEng.size() - 1) {
                    bundle = new Bundle();
                    bundle.putString("Subjects", str);
                    intent = new Intent(Game4_A.this, Games.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });

        findViewById(R.id.button_return_4a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game4_A.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Bundle bundle = new Bundle();
                                bundle.putString("Subjects", str);
                                Intent intent = new Intent(Game4_A.this, Games.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        findViewById(R.id.button_off_4a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game4_A.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Game4_A.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
