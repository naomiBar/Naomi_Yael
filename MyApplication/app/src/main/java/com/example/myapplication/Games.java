package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class Games extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);

        Bundle b = getIntent().getExtras();
        final String subject = b.getString("Subjects");
        final String listHeb = b.getString("listHeb");
        final String listEng = b.getString("listEng");
        final ArrayList<String> HebrewList = new  ArrayList<String>(Arrays.asList(listHeb.split(",")));
        final ArrayList<String> EnglishList = new  ArrayList<String>(Arrays.asList(listEng.split(",")));


       findViewById(R.id.button_game1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                bundle.putStringArrayList("HebrewList", HebrewList);
                bundle.putStringArrayList("EnglishList", EnglishList);
                Intent intent = new Intent(Games.this, Game1.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

       /** findViewById(R.id.button_game2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                bundle.putStringArrayList("HebrewList", listHeb);
                bundle.putStringArrayList("EnglishList", listEng);
                Intent intent = new Intent(Games.this, Game2.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_game3).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bundle bundle = new Bundle();
            bundle.putString("subjects", subject);
            bundle.putStringArrayList("HebrewList", listHeb);
            bundle.putStringArrayList("EnglishList", listEng);
            Intent intent = new Intent(Games.this, Game3.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        });

        findViewById(R.id.button_game4).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bundle bundle = new Bundle();
            bundle.putString("Subjects", subject);
            bundle.putStringArrayList("HebrewList", listHeb);
            bundle.putStringArrayList("EnglishList", listEng);
            Intent intent = new Intent(Games.this, Game4.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        });

        findViewById(R.id.button_game5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                bundle.putStringArrayList("HebrewList", listHeb);
                bundle.putStringArrayList("EnglishList", listEng);
                Intent intent = new Intent(Games.this, Game5.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_return_games).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Games.this, Subjects.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_off_games).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Games.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Games.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });**/
    }
}