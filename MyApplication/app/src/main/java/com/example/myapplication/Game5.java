package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game5);

        Bundle b = getIntent().getExtras();
        final String subject = b.getString("Subjects");
        final ArrayList<String> listHeb = b.getStringArrayList("HebrewList");
        final ArrayList<String> listEng = b.getStringArrayList("EnglishList");

        findViewById(R.id.button_start_5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                bundle.putInt("num", 0);
                bundle.putStringArrayList("HebrewList", listHeb);
                bundle.putStringArrayList("EnglishList", listEng);
                Intent intent = new Intent(Game5.this, Game5_A.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_return_5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                Intent intent = new Intent(Game5.this, Games.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_off_5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game5.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Game5.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
