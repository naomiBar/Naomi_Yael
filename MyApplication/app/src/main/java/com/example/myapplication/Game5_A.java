package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wafflecopter.charcounttextview.CharCountTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game5_A extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game5__a);

        Bundle b = getIntent().getExtras();
        final String str = b.getString("Subjects");
        final int j = b.getInt("num");
        final ArrayList<String> listHeb = b.getStringArrayList("HebrewList");
        final ArrayList<String> listEng = b.getStringArrayList("EnglishList");


        final TextView subject = findViewById(R.id.text_subject_5a);
        subject.setText("הנושא: " + str);

        TextView textHebrew = findViewById(R.id.text_hebrew_5a);
        textHebrew.setText(listHeb.get(j).toString());

        com.wafflecopter.charcounttextview.CharCountTextView g = findViewById(R.id.tvTextCounter_5a);
        g.setMaxCharacters((listEng.get(j)).toString().length());

        CharCountTextView charCountTextView = findViewById(R.id.tvTextCounter_5a);
        final EditText editText = findViewById(R.id.text_translate_5a);;
        charCountTextView.setEditText(editText);
        editText.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(listEng.get(j).toString().length())
        });

        charCountTextView.setCharCountChangedListener(new CharCountTextView.CharCountChangedListener() {
            @Override
            public void onCountChanged(int i, boolean b) {
            }
        });

        final Button button_ok = findViewById(R.id.button_ok_5a);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    //do what you want on the press of 'done'
                    button_ok.performClick();
                    //assertTrue(controller.isFinishing());
                }
                return false;
            }
        });

        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!((editText.getText().toString().toLowerCase()).equals(listEng.get(j).toString().toLowerCase()))) {
                    listHeb.add(listHeb.get(j));
                    listEng.add(listEng.get(j));
                    Toast.makeText(Game5_A.this, "תזכורת! " + listHeb.get(j).toString() + "=" +  listEng.get(j).toString(), Toast.LENGTH_LONG).show();
                }
                if (j == listEng.size() - 1) {
                    Bundle bundle = new Bundle();
                    bundle.putString("Subjects", str);
                    Intent intent = new Intent(Game5_A.this, Games.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", str);
                bundle.putInt("num", j + 1);
                bundle.putStringArrayList("HebrewList", listHeb);
                bundle.putStringArrayList("EnglishList", listEng);
                Intent intent = new Intent(Game5_A.this, Game5_A.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


        findViewById(R.id.button_return_5a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game5_A.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Bundle bundle = new Bundle();
                                bundle.putString("Subjects", str);
                                Intent intent = new Intent(Game5_A.this, Games.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        findViewById(R.id.button_off_5a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game5_A.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Game5_A.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}