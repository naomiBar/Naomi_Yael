package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;

public class Game1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game1);

        Bundle b = getIntent().getExtras();
        final String subject = b.getString("Subjects");
        final ArrayList<String> HebrewList = b.getStringArrayList("HebrewList");
        final ArrayList<String> EnglishList = b.getStringArrayList("EnglishList");

        findViewById(R.id.button_start_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                bundle.putStringArrayList("HebrewList", HebrewList);
                bundle.putStringArrayList("EnglishList", EnglishList);
                Intent intent = new Intent(Game1.this, Game1_A.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_return_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                Intent intent = new Intent(Game1.this, Games.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_off_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game1.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Game1.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
