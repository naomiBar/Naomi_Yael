package com.example.myapplication;

import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import java.lang.String;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Subjects extends AppCompatActivity {

    String subject = "";
    String listHeb = "";
    String listEng = "";


    //private ArrayList<String> listEng;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);


        findViewById(R.id.button_subject1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subject = "מאכלים";
                new Thread(new ClientThread()).start();
                String h = listHeb;
                Toast.makeText(Subjects.this, listHeb , Toast.LENGTH_LONG).show();
                Bundle b = new Bundle();
                b.putString("Subjects", subject);
                b.putString("listHeb", listHeb);
                b.putString("listEng", listEng);
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        /**findViewById(R.id.button_subject2).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "פירות";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", subject);
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject3).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "ירקות";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", subject);
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject4).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "ימי_השבוע";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", "ימי השבוע");
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject5).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "חיות";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", subject);
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject6).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "איברי_גוף";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", "איברי גוף");
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject7).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "כלי_מטבח";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", "כלי מטבח");
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject8).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "צבעים";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", subject);
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject9).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "מספרים";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", subject);
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject10).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "משפחה";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", subject);
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject11).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "פעולות";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", subject);
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject12).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "כלי_בית";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", "כלי בית");
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject13).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "בגדים";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", subject);
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject14).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "בית_חולים";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", "בית חולים");
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });

         findViewById(R.id.button_subject15).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
        subject = "כלי_תחבורה";
        new Thread(new ClientThread()).start();
        Bundle b = new Bundle();
        b.putString("Subjects", "כלי תחבורה");
        b.putStringArrayList("HebrewList", listHeb);
        b.putStringArrayList("EnglishList", listEng);
        Intent intent = new Intent(Subjects.this, Games.class);
        intent.putExtras(b);
        startActivity(intent);
        }
        });**/

        findViewById(R.id.button_return_subjects).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Subjects.this, Sign_In.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_off_subjects).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Subjects.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Subjects.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    class ClientThread implements Runnable {
        @Override
        public void run() {
            try {
                Client c = new Client("192.168.31.201", 9999);
                c.send(subject);

                listHeb = c.recv();
                listEng = c.recv();
            }

            catch (Exception e) {
                e.printStackTrace();
                System.out.println("thread error");
            }
        }
    }
}
