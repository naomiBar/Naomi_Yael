package com.example.myapplication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.Inet4Address;

/**
 * Created by User on 1/7/2018.
 */

public class Client {

    private Socket socket = null;
    private BufferedReader reader = null;
    private BufferedWriter writer = null;

    public Client(String address, int port) throws IOException
    {
        socket = new Socket(address,port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    public void send(String msg) throws IOException
    {
        writer.write(msg, 0, msg.length());
        writer.flush();
    }

    public String recv() throws IOException
    {
        return reader.readLine();
    }

}


/**package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.*;
import java.util.List;

public class Client extends AppCompatActivity {
    Socket server_socket = null;
    BufferedReader reader = null;
    BufferedWriter writer = null;
    String URL = "172.20.10.5";

    //String URL = "10.8.5.193";
    int PORT = 9999;
    PrintWriter out;
    BufferedReader input;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        new Thread(new ClientThread()).start();

        findViewById(R.id.logIn_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new ClientThread()).start();
            }
        });

    }

    class ClientThread implements Runnable {
        @Override
        public void run() {
            try {
                server_socket = new Socket(URL, PORT);
                reader = new BufferedReader(new InputStreamReader(server_socket.getInputStream()));
                writer = new BufferedWriter(new OutputStreamWriter(server_socket.getOutputStream()));
                writer.write("חיות", 0, "חיות".length());
                // s = "k";
                writer.flush();
                //s = reader.readLine();


                //.makeText(Client.this, s, Toast.LENGTH_SHORT).show();
                //out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(server_socket.getOutputStream())), true);
                //out.println(((EditText) findViewById(R.id.email)).getText().toString() + ((EditText) findViewById(R.id.pass)).getText().toString());

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("thread error");
                Toast.makeText(Client.this, "הגעת לסוף!" , Toast.LENGTH_LONG).show();
            }
        }
    }
}
**/



