package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class Game1_A extends AppCompatActivity {
    int i = 0;

    public void change(String word, final ArrayList<String> listHeb, final ArrayList<String> listEng){
        final TextView text = findViewById(R.id.text_src_1a);
        text.setText(word);

        findViewById(R.id.button_click_1a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(text.getText().toString() == listEng.get(i)){
                    text.setText(listHeb.get(i));
                }
                else{
                    text.setText(listEng.get(i));
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game1__a);

        Bundle b = getIntent().getExtras();
        final String str = b.getString("Subjects");
        final ArrayList<String> HebrewList = b.getStringArrayList("HebrewList");
        final ArrayList<String> EnglishList = b.getStringArrayList("EnglishList");

        final TextView subject = findViewById(R.id.text_subject_1a);
        subject.setText("הנושא: " + str);

        change(HebrewList.get(0), HebrewList, EnglishList);
        findViewById(R.id.imageButton1_1a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(i>=0 && i<(HebrewList.size())-1) {
                    i++;
                    change(HebrewList.get(i), HebrewList, EnglishList);
                }
                else
                {
                    Toast.makeText(Game1_A.this, "הגעת לסוף!" , Toast.LENGTH_LONG).show();
                }
            }
        });

        findViewById(R.id.imageButton2_1a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(i>0 && i<HebrewList.size()) {
                    i--;
                    change(HebrewList.get(i), HebrewList, EnglishList);
                }
                else {
                    Toast.makeText(Game1_A.this, "הגעת להתחלה!" , Toast.LENGTH_LONG).show();
                }
            }
        });

        findViewById(R.id.button_return_1a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game1_A.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Bundle bundle = new Bundle();
                                bundle.putString("Subjects", str);
                                Intent intent = new Intent(Game1_A.this, Games.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        findViewById(R.id.button_off_1a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game1_A.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Game1_A.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}