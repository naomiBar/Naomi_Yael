package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wafflecopter.charcounttextview.CharCountTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game5_A extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game5__a);

        Bundle b = getIntent().getExtras();
        final String str = b.getString("Subjects");
        final int j = b.getInt("num");
        final ArrayList<String> list1 = b.getStringArrayList("list_hebrew");
        final ArrayList<String> list2 = b.getStringArrayList("list_dst");

        final TextView subject = findViewById(R.id.text_subject_5a);
        subject.setText("הנושא: " + str);

        TextView textHebrew = findViewById(R.id.text_hebrew_5a);
        textHebrew.setText(list1.get(j).toString());

        com.wafflecopter.charcounttextview.CharCountTextView g = findViewById(R.id.tvTextCounter_5a);
        g.setMaxCharacters((list2.get(j)).toString().length());


        CharCountTextView charCountTextView = findViewById(R.id.tvTextCounter_5a);
        final EditText editText = findViewById(R.id.text_translate_5a);;
        charCountTextView.setEditText(editText);
        editText.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(list2.get(j).toString().length())
        });

        charCountTextView.setCharCountChangedListener(new CharCountTextView.CharCountChangedListener() {
            @Override
            public void onCountChanged(int i, boolean b) {
            }
        });

        findViewById(R.id.button_ok_5a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!((editText.getText().toString().toLowerCase()).equals(list2.get(j).toString().toLowerCase()))) {
                    list1.add(list1.get(j));
                    list2.add(list2.get(j));
                    Toast.makeText(Game5_A.this, "תזכורת! " + list1.get(j).toString() + "=" +  list2.get(j).toString(), Toast.LENGTH_LONG).show();
                }
                if (j == list2.size() - 1) {
                    Bundle bundle = new Bundle();
                    bundle.putString("Subjects", str);
                    Intent intent = new Intent(Game5_A.this, Games.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", str);
                bundle.putInt("num", j + 1);
                bundle.putStringArrayList("list_hebrew", list1);
                bundle.putStringArrayList("list_dst", list2);
                Intent intent = new Intent(Game5_A.this, Game5_A.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


        findViewById(R.id.button_return_5a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game5_A.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Bundle bundle = new Bundle();
                                bundle.putString("Subjects", str);
                                Intent intent = new Intent(Game5_A.this, Games.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        findViewById(R.id.button_off_5a).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game5_A.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Game5_A.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}