package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Subjects extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);

        findViewById(R.id.button_subject1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "מאכלים");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "פירות");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "ירקות");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "ימי השבוע");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "חיות");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "איברי גוף");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "כינויי גוף");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "צבעים");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "מספרים");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "משפחה");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject11).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "פעולות");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject12).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "אירועים");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject13).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "בגדים");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject14).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "מזג אוויר");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_subject15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("Subjects", "כלי תחבורה");
                Intent intent = new Intent(Subjects.this, Games.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_return_subjects).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Subjects.this, MainActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_off_subjects).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Subjects.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Subjects.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}