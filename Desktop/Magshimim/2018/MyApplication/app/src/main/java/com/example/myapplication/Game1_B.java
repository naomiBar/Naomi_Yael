package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class Game1_B extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game1__b);

        Bundle b = getIntent().getExtras();
        final String str = b.getString("Subjects");
        int number = b.getInt("number");
        final ArrayList<String> list = b.getStringArrayList("list_dst");

        TextView subject = findViewById(R.id.text_subject_1b);
        subject.setText("הנושא: " + str);

        TextView text = findViewById(R.id.text_dst_1b);
        text.setText(list.get(number));

        Button button = findViewById(R.id.button_goBack_1b);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        findViewById(R.id.button_off_1b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game1_B.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Game1_B.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}