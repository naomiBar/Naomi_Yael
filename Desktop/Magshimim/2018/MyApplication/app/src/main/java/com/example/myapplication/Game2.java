package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;

public class Game2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game2);

        Bundle b = getIntent().getExtras();
        final String subject = b.getString("Subjects");

        final ArrayList<String> list1 = new ArrayList<String>();
        list1.addAll(Arrays.asList("כלב", "חתול", "עכבר", "פיל", "חולדה", "דג"));
        final ArrayList<String> list2 = new ArrayList<String>();
        list2.addAll(Arrays.asList("Dog", "Cat", "Mouse", "Elephant", "Rat", "Fish"));

        findViewById(R.id.button_start_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                bundle.putStringArrayList("list_hebrew",list1);
                bundle.putStringArrayList("list_dst",list2);
                Intent intent = new Intent(Game2.this, Game2_A.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


        findViewById(R.id.button_return_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("Subjects", subject);
                Intent intent = new Intent(Game2.this, Games.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        findViewById(R.id.button_off_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Game2.this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        //YES
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                ActivityCompat.finishAffinity(Game2.this);
                            }
                        })
                        //No
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
