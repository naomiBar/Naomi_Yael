# -*- coding: utf-8 -*-
import sqlite3
from sqlite3 import Error


def create_tables(cur,subject):
    try:
        cur.execute("CREATE TABLE " + subject + "(hebrew string primary key, translate string)")
    except Error as e:
        print(e)


def addWords(cur,subject,words):
    try:
        print subject
        cur.executemany("INSERT INTO " + subject + " VALUES(?,?);",words)
    except Error as e:
        print(e)


def getLists():
    list = [[(unicode("בשר","UTF-8"),"meat"),(unicode("פסטה","UTF-8"),"pasta"),(unicode("עוף","UTF-8"),"chicken"),(unicode("ביצה","UTF-8"),"egg"),
              (unicode("שוקולד","UTF-8"),"chocolate"),(unicode("עוגיה","UTF-8"),"Cookie"),(unicode("סלט","UTF-8"),"salad"),(unicode("פיצה","UTF-8"),"pizza"),
              (unicode("גלידה","UTF-8"),"Ice cream"),(unicode("גבינה","UTF-8"),"cheese"),(unicode("חלב","UTF-8"),"milk"),(unicode("המבורגר","UTF-8"),"hamburger")],

             [(unicode("תפוז","UTF-8"),"orange"),(unicode("אפרסק","UTF-8"),"peach"),(unicode("אגס","UTF-8"),"pear"),(unicode("בננה","UTF-8"),"Banana"),
              (unicode("אבטיח","UTF-8"),"watermelon"),(unicode("תות","UTF-8"),"strawberry"),(unicode("קיווי","UTF-8"),"Kiwi"),(unicode("דובדבן","UTF-8"),"cherry"),
              (unicode("מנגו","UTF-8"),"mango"),(unicode("אשכולית","UTF-8"),"grapefruit"),(unicode("אננס","UTF-8"),"pineapple"),(unicode("ענבים","UTF-8"),"Grapes")],

             [(unicode("מלפפון","UTF-8"),"cucumber"),(unicode("עגבנייה","UTF-8"),"Tomato"),(unicode("גזר","UTF-8"),"Carrot"),(unicode("אבוקדו","UTF-8"),"Avocado"),
              (unicode("פלפל","UTF-8"),"pepper"),(unicode("חסה","UTF-8"),"lettuce"),(unicode("פטריה","UTF-8"),"mushroom"),(unicode("בצל","UTF-8"),"Onion"),
              (unicode("זית","UTF-8"),"olive"),(unicode("חציל","UTF-8"),"eggplant"),(unicode("סלק","UTF-8"),"beet"),(unicode("כרוב","UTF-8"),"cabbage")],

              ##############
             [(unicode("יום_ראשון","UTF-8"),"Sunday"),(unicode("יום_שני","UTF-8"),"Monday"),(unicode("יום_שלישי","UTF-8"),"Tuesday"),(unicode("יום_רביעי","UTF-8"),"Wednesday"),
              (unicode("יום_חמישי","UTF-8"),"Thursday"),(unicode("יום_שישי","UTF-8"),"Friday"),(unicode("יום_שבת","UTF-8"),"Saturday")],

             [(unicode("כלב","UTF-8"),"dog"),(unicode("חתול","UTF-8"),"cat"),(unicode("עכבר","UTF-8"),"mouse"),(unicode("חולדה","UTF-8"),"rat"),
              (unicode("אריה","UTF-8"),"lion"),(unicode("נמר","UTF-8"),"Tiger"),(unicode("קוף","UTF-8"),"monkey"),(unicode("כריש","UTF-8"),"shark"),
              (unicode("נמלה","UTF-8"),"ant"),(unicode("חמור","UTF-8"),"Donkey"),(unicode("ארנב","UTF-8"),"Rabbit"),(unicode("סוס","UTF-8"),"horse")],

             [(unicode("יד","UTF-8"),"Hand"),(unicode("רגל","UTF-8"),"Foot"),(unicode("גב","UTF-8"),"back"),(unicode("אוזן","UTF-8"),"ear"),
              (unicode("אף","UTF-8"),"Nose"),(unicode("עין","UTF-8"),"eye"),(unicode("שיניים","UTF-8"),"Teeth"),(unicode("לשון","UTF-8"),"Tongue"),
              (unicode("בטן","UTF-8"),"stomach"),(unicode("ראש","UTF-8"),"head"),(unicode("גרון","UTF-8"),"throat"),(unicode("צוואר","UTF-8"),"Neck")],

             [(unicode("כוס","UTF-8"),"glass"),(unicode("קולפן","UTF-8"),"peeler"),(unicode("סיר","UTF-8"),"pot"),(unicode("קערה","UTF-8"),"bowl"),
            (unicode("צלחת","UTF-8"),"plate"),(unicode("סכין","UTF-8"),"knife"),(unicode("מזלג","UTF-8"),"fork"), (unicode("כפית","UTF-8"),"teaspoon"),
            (unicode("כף","UTF-8"),"tablespoon"),(unicode("מסננת","UTF-8"),"strainer"),(unicode("מיקסר","UTF-8"),"mixer"),(unicode("טוסטר","UTF-8"),"toaster")],

             [(unicode("אדום","UTF-8"),"red"),(unicode("כחול","UTF-8"),"blue"),(unicode("לבן","UTF-8"),"white"),(unicode("ירוק","UTF-8"),"green"),
              (unicode("סגול","UTF-8"),"purple"),(unicode("כתום","UTF-8"),"orange"),(unicode("שחור","UTF-8"),"black"),(unicode("צהוב","UTF-8"),"yellow"),
              (unicode("בורדו","UTF-8"),"Bordeaux"),(unicode("אפור","UTF-8"),"gray"),(unicode("ורוד","UTF-8"),"pink"),(unicode("זהב","UTF-8"),"gold")],

             [(unicode("אפס","UTF-8"),"zero"),(unicode("אחת","UTF-8"),"One"),(unicode("שתיים","UTF-8"),"two"),(unicode("שלוש","UTF-8"),"three"),
              (unicode("ארבע","UTF-8"),"Four"),(unicode("חמש","UTF-8"),"five"),(unicode("שש","UTF-8"),"six"),(unicode("שבע","UTF-8"),"seven"),
              (unicode("שמונה","UTF-8"),"eight"), (unicode("תשע","UTF-8"),"nine"),(unicode("עשר","UTF-8"),"nine")],

             [(unicode("סבא","UTF-8"),"grandfather"),(unicode("סבתא","UTF-8"),"grandmother"),(unicode("אמא","UTF-8"),"mother"),(unicode("אבא","UTF-8"),"father"),
              (unicode("דוד","UTF-8"),"Uncle"),(unicode("דודה","UTF-8"),"aunt"),(unicode("אח","UTF-8"),"brother"),(unicode("אחיין","UTF-8"),"nephew"),
              (unicode("ילד","UTF-8"),"Boy"),(unicode("ילדה","UTF-8"),"Girl"),(unicode("נכד","UTF-8"),"grandson"), (unicode("נכדה","UTF-8"),"granddaughter")],

            [(unicode("אכילה","UTF-8"),"eating"),(unicode("שתיה","UTF-8"),"drinking"),(unicode("הליכה","UTF-8"),"walk"),(unicode("ריצה","UTF-8"),"running"),
             (unicode("כתיבה","UTF-8"),"writing"),(unicode("קריאה","UTF-8"),"reading"),(unicode("שינה","UTF-8"),"Sleep"),(unicode("קפיצה","UTF-8"),"spring"),
             (unicode("זריקה","UTF-8"),"throwing"),(unicode("שחייה","UTF-8"),"Swimming"),(unicode("נסיעה","UTF-8"),"travel"),(unicode("ריקוד","UTF-8"),"Dance")],

             [(unicode("ספה","UTF-8"),"sofa"),(unicode("תנור","UTF-8"),"Oven"),(unicode("קיר","UTF-8"),"wall"),(unicode("דלת","UTF-8"),"door"),
             (unicode("שולחן","UTF-8"),"Table"),(unicode("כסא","UTF-8"),"chair"),(unicode("מנורה","UTF-8"),"lamp"),(unicode("מקלחת","UTF-8"),"shower"),
             (unicode("חלון","UTF-8"),"Window"),(unicode("ארון","UTF-8"),"Cabinet"),(unicode("רצפה","UTF-8"),"floor"),(unicode("מחשב","UTF-8"),"computer")],

             [(unicode("חולצה","UTF-8"),"shirt"),(unicode("מכנסיים","UTF-8"),"Shorts"),(unicode("חצאית","UTF-8"),"Skirt"),(unicode("שמלה","UTF-8"),"dress"),
             (unicode("גרביים","UTF-8"),"Socks"),(unicode("כפפות","UTF-8"),"Gloves"),(unicode("צעיף","UTF-8"),"scarf"),(unicode("כובע","UTF-8"),"hat"),
             (unicode("גופיה","UTF-8"),"undershirt"),(unicode("מעיל","UTF-8"),"Coat"),(unicode("מגפיים","UTF-8"),"boots"),(unicode("חגורה","UTF-8"),"Belt")],

             [(unicode("אלונקה","UTF-8"),"stretcher"),(unicode("מרפאה","UTF-8"),"clinic"),(unicode("אחות","UTF-8"),"nurse"),(unicode("מנתח","UTF-8"),"surgeon"),
             (unicode("רופא","UTF-8"),"doctor"),(unicode("הרדמה","UTF-8"),"anesthesia"),(unicode("לידה","UTF-8"),"birth"),(unicode("תרופה","UTF-8"),"medication"),
             (unicode("תחבושת","UTF-8"),"Bandage"),(unicode("חולה","UTF-8"),"Sick"),(unicode("מזרק","UTF-8"),"syringe"),(unicode("חובש","UTF-8"),"medic")],

             [(unicode("אוטובוס","UTF-8"),"bus"),(unicode("לימוזינה","UTF-8"),"limousine"),(unicode("מכונית","UTF-8"),"car"),(unicode("אופנוע","UTF-8"),"motorcycle"),
             (unicode("אופניים","UTF-8"),"bicycle"),(unicode("משאית","UTF-8"),"truck"),(unicode("סירה","UTF-8"),"boat"),(unicode("מסוק","UTF-8"),"helicopter"),
             (unicode("טרקטור","UTF-8"),"tractor"),(unicode("אמבולנס","UTF-8"),"ambulance")]]
    return list


def getListHebrew(cur,subject):
    try:
        cur.execute("select hebrew from " + subject + ";")
    except Error as e:
        print(e)
    list = []
    for i in cur.fetchall():
        for j in i:
            list.append(j)
    return list


def getListTranslate(cur,subject):
    try:
        cur.execute("select translate from " + subject + ";")
    except Error as e:
        print(e)
    list = []
    for i in cur.fetchall():
        for j in i:
            list.append(j)
    return list


def main(db_file):
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()
    subjects = [unicode("מאכלים","UTF-8"), unicode("פירות","UTF-8"), unicode("ירקות","UTF-8"), unicode("ימי_השבוע","UTF-8"), unicode("חיות","UTF-8"),
                unicode("איברי_גוף","UTF-8"), unicode("כלי_מטבח","UTF-8"), unicode("צבעים","UTF-8"), unicode("מספרים","UTF-8"), unicode("משפחה","UTF-8"),
                unicode("פעולות","UTF-8"), unicode("כלי_בית","UTF-8"), unicode("בגדים","UTF-8"), unicode("בית_חולים","UTF-8"), unicode("כלי_תחבורה","UTF-8")]
    list = getLists()

    g = 0
    for subject in subjects:
        create_tables(cur,subject)
        addWords(cur,subject,list[g])
        g = g+1

    list_hebrew = getListHebrew(cur,unicode("כלי_בית","UTF-8"))
    '''for word in list_hebrew:
        print word'''
    list_translate = getListTranslate(cur,unicode("כלי_בית","UTF-8"))
    '''for word in list_translate:
        print word'''

    conn.commit()
    conn.close()


if __name__ == '__main__':
    main("f.db")